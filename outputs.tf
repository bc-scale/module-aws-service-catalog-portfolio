output "id" {
  description = "ID of the portfolio."
  value       = aws_servicecatalog_portfolio.this.id
}
